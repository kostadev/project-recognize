#!/usr/bin/python

# Import the modules needed to run the script.
import sys
from FaceRecognizer import FaceRecognizer as fr
import Camera as cam

import SpeechGenerator

# Main definition - constants
menu_actions  = {}

# Speech generator init
sg = SpeechGenerator.SpeechGenerator()
sg.say("Loading Data")

# Recognizer used
recognizer = fr()
recognizer.load(anyway=True)
sg.say("Recognizer Loaded")
 
# =======================
#     MENUS FUNCTIONS
# =======================
 
# Main menu
def main_menu():
    
    print ("\nPlease choose action:")
    print ("1. Load and Save")
    print ("2. Test Face Detection")
    print ("3. Take Pictures")
    print ("4. Say something")
    print ("5. Upload From USB")
    print ("\n0. Quit")
    choice = input(" >>  ")
    exec_menu(choice)
 
    return
 
# Execute menu
def exec_menu(choice):
    #os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print ("Invalid selection, please try again.\n")
            menu_actions['main_menu']()
    return
 
# Menu 1
def menu1():
    print ("1. Load from XML")
    print ("2. Save to XML")
    print ("3. Load from Folder")
    print ("4. Update from Folder")
    print ("9. Back\n")
    
    
    choice = input(" >>  ")
    if choice == "1":
        print("Loading from XML")
        recognizer.load()
    elif choice == "2":
        print("Saving to XML...")
        recognizer.save()
    elif choice == "3":
        print ("Loading Database...")
        recognizer.learn_faces()
    elif choice == "4":
        print ("Updating Database...")
        recognizer.learn_faces(update=True)
    
    main_menu()
    

# Menu 2
def menu2():
    print ("Detecting Faces Test \n")
    result = recognizer.find_faces(path="./inputpics/test",testmode=True)
    sg.say(result)
    
    main_menu()
    
# Menu 3
def menu3():
    print ("1. Take Training Picture Set")
    print ("2. Take Image to Recognize")
    print ("9. Back\n")
    
    choice = input(" >>  ")
    if choice == "1":
        camera = cam.USBCamera()
        camera.take_training_set()
    elif choice == "2":        
        camera = cam.USBCamera()
        if camera.take_picture(pic_path='inputpics'):
            result = recognizer.find_faces()
            sg.say(result)
    
    main_menu()

# Menu 4
def menu4():
    
    sg = SpeechGenerator.SpeechGenerator()
    sg.sayRandom()
    
    main_menu()

# Menu 5
def menu5():
    from FileHelper import UsbDetector
    usb = UsbDetector()
    usb.start()
    recognizer.learn_faces(update=True)
    main_menu()
    

# Back to main menu
def back():
    menu_actions['main_menu']()
 
# Exit program
def exit():
    sys.exit()
 
# =======================
#    MENUS DEFINITIONS
# =======================
 
# Menu definition
menu_actions = {
    'main_menu': main_menu,
    '1': menu1,
    '2': menu2,
    '3': menu3,
    '4': menu4,
    '5': menu5,
    '9': back,
    '0': exit,
}
 
 
# =======================
#      MAIN PROGRAM
# =======================
 
# Main Program
if __name__ == "__main__":
    # Launch main menu
    main_menu()
