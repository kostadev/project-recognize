#!/usr/bin/python
import os
import FileHelper as fh

class USBCamera():
    
    def __init__(self):
        self.__extention = "pgm"
        self.__emotions = ['glasses','noglasses','happy','normal','sad','rightlight','leftlight','sleepy','surprised','wink']
    
    def take_training_set(self):
        path="images"
        print ("Create New or Add to existing")
            
        name = input("Name : >> ")
        label_in = input("Label (numeric) : >> ")
        #folder_path = "./{}/{}_{}".format(path,name,label) # test_1
        
        path = "{}/{}".format(path,label_in)        
        
        if not os.path.exists(path):
            os.makedirs(path)
            
        for emotion in self.__emotions:
            st_in = input("Recomendation : {} \n Take picture? y/n/c   >> ".format(emotion))
            if "y" == st_in:
                label = "{}.{}.{}".format(name,label_in,emotion) 
                self.take_picture(pic_name=label,pic_path=path)
            elif "c" == st_in:
                print("Canceled")
                break
            
                    
    def take_picture(self, pic_name="new_picture", pic_path="inputpics"):
        
        folder_path = "./{}".format(pic_path)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
            
        st_in = input("Ready to Take picture? \n > {} \nHit Enter or c to Cancel\n >> ".format(pic_name))
        if not "c" == st_in:
            full_label = "./{}/{}.{}".format(pic_path,pic_name,self.__extention)
            helper = fh.FileHelper()
            helper.takePicture(full_label)
            return True
        