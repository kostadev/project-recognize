#!/usr/bin/python
import os
import random

class SpeechGenerator():
    def __init__(self,speed=140,pitch=40):
        self.__voice = "en+f4"  #english female option 4
        self.__pitch = pitch   #0..99
        self.__speed = speed  #words per minute
        pass
    
    def say(self,text):
        print("Saying: {}".format(text))
        os.system( 'espeak -v {} -p {} -s {} "{}" 2> /dev/null'.format(self.__voice,self.__pitch,self.__speed,text))
        
    def sayRandom(self):
        options = [
            "Say hello to my little friend!", 
            "They call it a Royale with cheese.", 
            "If you build it, he will come",
            "Elementary, my dear Watson",
            "Hasta la vista, baby",
            "It's alive! It's alive!",
            "Help me, Obi-Wan Kenobi. You're my only hope.",
            "Why so serious?"]
        self.say(options[random.randint(0,len(options)-1)])
    