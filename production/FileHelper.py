#!/usr/bin/python

import os
import numpy as np
from PIL import Image
from subprocess import Popen

from pyudev import Context, Monitor, MonitorObserver
from glob import glob
import psutil
import time
from shutil import copyfile


class FileHelper():
    def __init__(self):
        pass
    
    def getImageNameNbr(self, real_image_path):
        image_pil = Image.open(real_image_path).convert('L')
        # Convert the image format into numpy array
        image = np.array(image_pil, 'uint8')
        # Get the name of the image        
        name = os.path.split(real_image_path)[1].split(".")[0]
        # Get the label of the image
        nbr = int(os.path.split(real_image_path)[1].split(".")[1])
        return image,name,nbr
        
    def getImage(self, real_image_path):
        image_pil = Image.open(real_image_path).convert('L')
        image = np.array(image_pil, 'uint8')
        return image
                    
    def saveData(self,names,images):
        
        f = open("names.txt","w")
        for label,name in names.items():
            f.write("{}:::{}\n".format(label,name))
        f.close()
        f = open("imagepaths.txt","w")
        for path in images:
            f.write("{}\n".format(path))
        f.close()
        print("Data saved to XML")
        
    def loadData(self):
        names = {}
        images = []
        try:
            f = open("names.txt","r")
            for line in f:
                ln = line.replace("\n","").split(":::")
                names[int(ln[0])] = ln[1]
            print("People : {}".format(len(names)))
            f.close()
        except:
            print("names.txt is missing or invalid")
            
        try:
            f = open("imagepaths.txt","r")
            for line in f:                
                images.append(line.replace("\n",""))
            print("Images : {}".format(len(images)))
            f.close()
        except:
            print("imagepaths.txt is missing or invalid")
            
        return names,images
    
    def takePicture(self,label):
        
        extention = label.split(".")[-1]
        emotion   = label.split(".")[-2]
        if emotion == "sad":
            holder = label.split("/")
            holder[1] = "inputpics"
            holder[2] = "test"
            label = "/".join(holder)
            print("path: {}".format(label))
        process = Popen("streamer -f {} -o {}".format(extention,label), shell=True)
        if 1 == process.wait():
            print("\nCamera Error. Check Connection \n")
        else:
            print("\nPicture {} taken \n".format(label))
    
    def cleanFolder(self,path="inputpics"):
        image_paths = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        for image_path in image_paths:
            try:
                os.unlink("./{}/{}".format(path,image_path))
            except Exception as e:
                print(e)
                

class UsbDetector():
    def __init__(self):
        self.context = Context()
        self.monitor = Monitor.from_netlink(self.context)
        self.monitor.filter_by(subsystem='usb')
        self.observer = MonitorObserver(self.monitor, callback=self.device_event, name='monitor-observer')
        

    def device_event(self,device):
        if device.action == 'add' and int(device.device_number) != 0:
            print("Device Inserted, Waiting to Mount...")        
            time.sleep(5)
            counter = 0
            partitions = [dev.device_node for dev in self.context.list_devices(subsystem='block', DEVTYPE='partition', parent=device)]
            print("All removable partitions: {}".format(", ".join(partitions)))
            print("Mounted removable partitions:")
            for p in psutil.disk_partitions():
                if p.device in partitions:
                    print("  {}: {} ".format(p.device, p.mountpoint))
                    #print(glob("{}/*/".format(p.mountpoint)))
                    for pic in glob("{}/pictures/*.*.*".format(p.mountpoint)):
                        filename = pic.split("/")[-1]
                        folder = "./images/{}".format(filename.split(".")[1])
                        if not os.path.exists(folder):
                            os.makedirs(folder)
                        # copy files to program folder
                        new_file_path = "{}/{}".format(folder,filename)
                        if not os.path.exists(new_file_path):
                            copyfile(pic, new_file_path)
                            counter +=1
                       
            print(" {} photos uploaded ".format(counter))
            self.stop()
            
    def start(self):   
        
        print("\nUsb Monitor Started")
        print("Insert USB Now")
        print("Press CTRL+C to Stop\n")
        self.observer.start()
        try:
            while self.observer.is_alive():
                self.observer.join(0.1)
        except KeyboardInterrupt:
            self.stop()
    
    def stop(self):
        print("\nUsb Monitor Stoped\n")
        self.observer.stop()