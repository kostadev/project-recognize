#!/usr/bin/python

# Import the required modules
import cv2, os
import numpy as np
import FileHelper as fh

#TODO Comments and code inspection
#     Final report

class FaceRecognizer():
    
    def __init__(self):
        # Path to the Yale Dataset
        self.__path_db = './images'
        # For face detection we will use the Haar Cascade provided by OpenCV.
        self.__faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
        # For face recognition we will use the LBPH Face Recognizer (to be updatable)
        self.__recognizer = cv2.face.createLBPHFaceRecognizer()
        # Dictionary to store labels and names
        self.__names = {}
        # Images that have been loaded already
        self.__images_loaded = []
        # Top confidence is a max distance at which we can assure similarity
        self.__top_conf = 150
        
    
    def get_images_and_labels(self, update=False):
        # Append all the absolute image paths in a list image_paths
        # We will not read the image with the .sad extension in the training set
        # Rather, we will use them to test our accuracy of the training
        folder_paths = [os.path.join(self.__path_db, f) for f in os.listdir(self.__path_db)]
        # images will contains face images
        images = []
        # labels will contains the label that is assigned to the image
        labels = []
        helper = fh.FileHelper()
        for folder_path in folder_paths:
            if(os.path.isdir(folder_path)):
                image_paths = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f)) and not f.endswith('.sad') ]
                if update:
                    image_paths = [p for p in image_paths if p not in self.__images_loaded]
                    
                for image_path in image_paths:                    
                    # Read the image and convert to grayscale (in case of USB uploaded files)
                    real_image_path = "./{}/{}".format(folder_path,image_path)
                    image,name,nbr = helper.getImageNameNbr(real_image_path) 
                    # Add name and label to dictionary
                    self.__names[nbr] = name
                    # Detect the face in the image
                    faces = self.__faceCascade.detectMultiScale(image)
                    # If face is detected, append the face to images and the label to labels
                    for (x, y, w, h) in faces:
                        images.append(image[y: y + h, x: x + w])
                        labels.append(nbr)
                        #cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
                        #cv2.waitKey(10)
                    self.__images_loaded.append(image_path)
                print("Folder {} Loaded".format(folder_path))
        # return the images list and labels list
        return images, labels
    
    def learn_faces(self, update=False):
        # Perform the tranining
        print("Performing training...")
        if not update:
            try:
                # Call the get_images_and_labels function and get the face images and the 
                # corresponding labels
                print("Loading faces to traning set...")
                images, labels = self.get_images_and_labels()
                print("Total Images Loaded : {}".format( len(labels) ))
                print("Total People in DB  : {}".format( len(self.__names) ))
                self.__recognizer.train(images, np.array(labels))
                print("Training finished")
            except:
                print("Not enough images in set to train recognizer")
        else:
            print("Adding faces to traning set...")
            images, labels = self.get_images_and_labels(update)
            self.__recognizer.update(images, np.array(labels))
            print("Learned {} New Images".format( len(labels) ))
            print("Total People in DB  : {}".format( len(self.__names) ))
            self.save()
    
    def find_faces(self, path="inputpics", testmode=False): 
        # Append the images with the extension .sad into image_paths
        image_paths = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        print("Recognition in process...")
        helper = fh.FileHelper()
        posit,negat = 0,0 # for testing
        for image_path in image_paths:
            real_image_path = "./{}/{}".format(path,image_path)
            predict_image = helper.getImage(real_image_path)
            
            faces = self.__faceCascade.detectMultiScale(predict_image)
            if len(faces) == 0:
                print("No face is detected")
            
                        
            
            for (x, y, w, h) in faces:
                
                result = cv2.face.MinDistancePredictCollector()
                try:
                    self.__recognizer.predict(predict_image[y: y + h, x: x + w],result, 0)
                except:
                    print("\nRecognizer is not trained yet")
                    return
                               
                nbr_predicted = int(result.getLabel())
                conf = result.getDist()
                
                pic_name = os.path.split(image_path)[1].split(".")[0]

                if testmode:
                    nbr_actual = int(os.path.split(image_path)[1].split(".")[1])
                    
                    if nbr_actual == nbr_predicted:
                        posit += 1
                        print( "{} is Correctly Recognized \n as {} {} \n confidence {}".format(pic_name,self.__names[nbr_predicted],nbr_predicted,conf) ) #
                    else:
                        negat += 1
                        print( "{} is Incorrectly Recognized \n as {} \n confidence {}".format(nbr_actual,nbr_predicted,conf) )
                else:
                    if conf > 150: conf = 150
                    new_conf = round((self.__top_conf - conf)/self.__top_conf * 100, 2)
                    result = "{} is Recognized \n as {} \n confidence {} percent".format(pic_name,self.__names[nbr_predicted],new_conf)
                    print(result)
                    helper.cleanFolder()
                    return result
        
        
        print("Recognition Finished")        
        
        if testmode:
            result = "{} out of {} recognized correctly".format(posit,(posit+negat))            
            return result
        
    def save(self):
        self.__recognizer.save("face_recog_data.xml")
        helper = fh.FileHelper()
        helper.saveData(self.__names, self.__images_loaded)
        
    def load(self,anyway=True):
        try:
            self.__recognizer.load("face_recog_data.xml")
            print("XML file loaded")
            helper = fh.FileHelper()
            self.__names, self.__images_loaded = helper.loadData()
            
        except:
            print ("XML file not found")
            if anyway:
                print ("Loading from Images folder")
                self.learn_faces(update=True)
                self.save()
